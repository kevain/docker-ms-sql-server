# Docker configuration for locally hosting Microsoft SQL Server (developer edition)

## Commands:

### Run container
```
docker-compose up -d
```

### Check running instances 
```
docker-compose ps
```

### Shut down container
```
docker-compose down
```
